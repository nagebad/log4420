 var pagesAppFilter = angular.module('pagesAppFilter', []);
 
  //LameToFancy
 pagesAppFilter.filter('ltF', function() {
    return function(input) { 
      var espace = (!!input) ? input.replace(/([a-z])([A-Z])/g, '$1 $2') : undefined;
      var capitalized = (!!espace) ? espace.charAt(0).toUpperCase() + espace.substr(1) : '';
      return capitalized;
    }
}); 