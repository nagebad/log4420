var creationJoueurApp = angular.module('creationJoueurApp', []);

creationJoueurApp.controller('creationJoueurController', function ($scope, $http, $q, $window) {

$scope.joueursExistant = [];

$scope.MSG_ERREUR = 
{
	NOM : "Veuillez ecrire un nom!",
    DISCIPLINE : "Veuillez choisir exactement 5 discipline",
    ARME : "Veuillez choisir exactement 2 items",
    MAITRISE : "Vous ne pouvez pas choisir une arme si vous n'avez pas la discipline de maitrise d'arme"
};	
	
$scope.master = {
	nom: "",
	disciplines:
				{
					camouflage: true,
					chasse: true,
					sixiemeSens: true,
					orientation: true,
					guerison: true,
					maitriseArmes: false,
					bouclierPsychique: false,
					puissancePsychique: false,
					communicationAnimale: false,
					maitrisePhysiqueMatiere: false
				},
    armesEtObjets:
    {
        epee : false,
        sabre : false,
        lance : false,
        masse : false,
        marteau : false,
        hache : false,
        baton : false,
        glaive : false,
        gilet : false,
        potion : true,
        ration : true

    }
	
	};

	
	$scope.showDisciplineError = false;
	$scope.showMaitriseArmeError = false;
	var disciplineCount =  5; //on commence avec 5 check dans disiplines
	
	aChoisisArme = function(){
		return (
		$scope.user.armesEtObjets.epee || $scope.user.armesEtObjets.sabre || $scope.user.armesEtObjets.lance ||
		$scope.user.armesEtObjets.masse || $scope.user.armesEtObjets.marteau || $scope.user.armesEtObjets.hache ||
		$scope.user.armesEtObjets.baton || $scope.user.armesEtObjets.gaive);
	}

	$scope.countDisciplines = function ($event) {
        var chkBox = $event.target;

		if(chkBox.checked){
            disciplineCount = disciplineCount + 1;
        }else{
            disciplineCount = disciplineCount - 1;
        }
        $scope.showDisciplineError = (disciplineCount !== 5);
        $scope.showMaitriseArmeError = aChoisisArme() ? !$scope.user.disciplines.maitriseArmes : false;
        
	};

    $scope.showArmeError = false;
    var armeCount =  2; //on commence avec 2 check dans armes

    $scope.countArme = function ($event) {
        var chkBox = $event.target;

        if(chkBox.checked){
            armeCount = armeCount + 1;
        }else{
            armeCount = armeCount - 1;
        }
        $scope.showArmeError = (armeCount !== 2);
        $scope.showMaitriseArmeError = aChoisisArme() ? !$scope.user.disciplines.maitriseArmes : false;      
    };



    $scope.reset = function() {
        $scope.user = angular.copy($scope.master);
    };
    
    $scope.deleteJoueur = function(joueur){
		$http.delete("/api/joueurs/" + joueur._id).success(
		function(data){
			$scope.joueursExistant = $scope.joueursExistant.filter(function (el) {
                        return el._id !== joueur._id;
                       });
		});
	}
	
    $scope.continuer = function(joueur){
		$http.get("/api/joueurs/continuer/" + joueur._id).success(
		function(data){
			$window.location.href = data.redirect; 
		});
	}	
	
	
    
	$http.get("/api/joueurs")
		.success(function (joueurs) 
		{
			angular.forEach(joueurs, function(joueur){
				$http.get("/api/joueurs/avancement/" +joueur._id)
				.success(function(avancement){
					joueur.avancement = avancement;
					$scope.joueursExistant.push(joueur);
				}); 
				
			
			});
		});
			
    $scope.reset();
});
