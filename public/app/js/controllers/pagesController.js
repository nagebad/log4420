pagesApp.controller('pagesController', function($scope, $http) {

    $scope.joueur = {};
    $scope.aDecision = false;
    $scope.aCombat = false;
    $scope.aConfirmation = false;
    $scope.round = 0;
    $scope.htmlPage = [];
    $scope.combat = {rondes: []};
    $scope.playerIsAlive = true;
    $scope.aleatoire134 = -10;
    $scope.aleatoire167 = -10;
    $scope.aleatoire331 = -10;
    $scope.aleatoire155 = -10;
    $scope.sectionPage = 0;
    $scope.pickup = "";
    $scope.pickupSpecial = [];
    $scope.pageData = {};
    $scope.avancement = {};

    updateAlive = function() {
        $scope.playerIsAlive = $scope.joueur.endurancePlus > 0;

        if ($scope.playerIsAlive === false) {
            $http.delete("/api/joueurs/" + $scope.joueur._id);

        }
    };

    getAvancementJoueur = function() {
        $http.get('../api/joueurs/courant').success(function(joueur) {
            $scope.joueur = joueur;
            updateAlive();
            $http.get('../api/joueurs/avancement/' + joueur._id).
                    success(function(data, status, headers, config) {
                        $scope.avancement = data;
                        $scope.numeroPage = data.pageId;
                        $scope.sectionPage = data.sectionId;
                        getPageHtml();
                    }).
                    error(function(data, status, headers, config) {
                        console.log("Error get avancement: " + status);
                    });
        });
    };

    $scope.enDuranceString = function() {
        var end = $scope.joueur.enduranceBase;
        var aGilet = _.contains($scope.joueur.objetsSpeciaux, "giletCuirMartele");
        var bonus = aGilet ? 2 : 0;

        return end + " + " + bonus + " = " + (end + bonus);
    };

    $scope.habileteString = function() {
        var hab = $scope.joueur.habileteBase;
        var bonus = $scope.joueur.habiletePlus - $scope.joueur.habileteBase;
        //var bonus = _.contains($scope.joueur.disciplines, "maitriseArmes") && !_.isEmpty($scope.joueur.armes) ? 2 : -4;
        //var bonus2 = $scope.combat.deltaHab;
        var op = " + ";
        if (bonus < 0) {
            op = " - ";
        }

        return hab + op + Math.abs(bonus) + " = " + (hab + bonus);
    };

    getPageHtml = function() {
        $scope.aDecision = false;
        $scope.pickup = undefined;
        $scope.pickupSpecial = [];
        $scope.aConfirmation = false;

        $http.get('../api/pages/' + $scope.numeroPage + '/' + $scope.sectionPage + '').
                success(function(data, status, headers, config) {
                    $scope.pageData = data;
                    if (data.contenu) {
                        for (var i = 0; i < data.contenu.length; ++i) {
                            $scope.htmlPage.push(data.contenu[i]);
                        }
                    }
                    
                    $scope.pickup = undefined !== data.objetTraineATerre ? data.objetTraineATerre : "";
                    $scope.pickupSpecial = undefined !== data.ajouterObjets ? data.ajouterObjets.items : [];

                    $scope.aCombat = undefined !== data.combat ? true : false;
                    if ($scope.aCombat) {
                        $scope.round = 1;
                        $scope.combat = {deltaHab: 0, peutFuir: data.combat.peutFuir, enVie: true, endurance: data.combat.endurance, habilete: data.combat.habilete, nom: data.combat.nom, rondes: [], combat: "combat"};
                        $scope.combat.aPP = $scope.joueur.disciplines.indexOf(discipline.PUISSANCE_PSYCHIQUE) > 0 ? true : false;
                        $scope.combat.totalDomage = 0;
                        $scope.htmlPage.push($scope.combat);
                    }
                    if (data.decisionLien === "/page/choixPossible" || data.decision === "/page/choixPossible")
                        getDecisions();
                    else if (data.decisionLien === "/page/choixAleatoire" || data.decision === "/page/choixAleatoire")
                        getChoixAleatoire();
                    else if (data.confirmation === "/page/confirmation")
                        if($scope.numeroPage === 155){
                            $scope.aConfirmation155 = true;
                        }
                        else{
                            $scope.aConfirmation = true;
                        }
                }).
                error(function(data, status, headers, config) {
                    console.log("Error get page: " + status);
                });
    };

    getDecisions = function() {
        $http.get('../api/pages/decision/' + $scope.numeroPage).
                success(function(data, status, headers, config) {
                    $scope.aDecision = true;
                    $scope.decisions = [];
                    
                    //ugh
                    if($scope.numeroPage === 180){
                        if($scope.avancement.dommage180 === 0 || $scope.combat.totalDomage === 0){
                            $scope.decisions.push(data[0]);
                        }else{
                            $scope.decisions.push(data[1]);
                        }
                    }
                    else{
                        for (var i = 0; i < data.length; ++i) {
                            if (data[i].isValid == true)
                                $scope.decisions.push(data[i]);
                        }
                    }
                }).
                error(function(data, status, headers, config) {
                    console.log("Error get decisions: " + status);
                });
    };

    afficherChoixAleatoires = function() {
        $http.get('../api/pages/choixAleatoire/' + $scope.numeroPage).
                success(function(data, status, headers, config) {
                    $scope.aDecision = true;
                    $scope.decisions = [];
                    if ($scope.numeroPage == 134) {
                        for (var i = 0; i < data.length; ++i) {
                            if (data[i].valid == true && $scope.aleatoire134 == -10) {
                                $scope.decisions.push(data[i]);
                                $scope.aleatoire134 = data[i].valeurAleatoire;
                                $http.put('../api/joueurs/avancement/' + $scope.joueur._id, {aleatoire134: $scope.aleatoire134});
                            }
                            else if ($scope.aleatoire134 >= data[i].min && $scope.aleatoire134 <= data[i].max)
                                $scope.decisions.push(data[i]);
                        }
                    }
                    else if ($scope.numeroPage == 167) {
                        for (var i = 0; i < data.length; ++i) {
                            if (data[i].valid == true && $scope.aleatoire167 == -10) {
                                $scope.decisions.push(data[i]);
                                $scope.aleatoire167 = data[0].valeurAleatoire;
                                $http.put('../api/joueurs/avancement/' + $scope.joueur._id, {aleatoire167: $scope.aleatoire167});
                            }
                            else if ($scope.aleatoire167 >= data[i].min && $scope.aleatoire167 <= data[i].max)
                                $scope.decisions.push(data[i]);
                        }
                    }
                    else if ($scope.numeroPage == 331) {
                        for (var i = 0; i < data.length; ++i) {
                            if (data[i].valid == true && $scope.aleatoire331 == -10) {
                                $scope.decisions.push(data[i]);
                                $scope.aleatoire331 = data[i].valeurAleatoire;
                                $http.put('../api/joueurs/avancement/' + $scope.joueur._id, {aleatoire331: $scope.aleatoire331});
                            }
                            else if ($scope.aleatoire331 >= data[i].min && $scope.aleatoire331 <= data[i].max)
                                $scope.decisions.push(data[i]);
                        }
                    }
                    else if ($scope.numeroPage == 155) {
                        for (var i = 0; i < data.length; ++i) {
                            if (data[i].valid == true && $scope.aleatoire155 == -10) {
                                $scope.decisions.push(data[i]);
                                $scope.aleatoire155 = data[i].valeurAleatoire;
                                $http.put('../api/joueurs/avancement/' + $scope.joueur._id, {aleatoire155: $scope.aleatoire155});
                            }
                            else if ($scope.aleatoire155 >= data[i].min && $scope.aleatoire155 <= data[i].max)
                                $scope.decisions.push(data[i]);
                        }
                    }
                }).
                error(function(data, status, headers, config) {
                    console.log("Error get decisions: " + status);
                });
    }

    getChoixAleatoire = function() {
        //on va chercher valeurs aleatoire deja obtenues
        $http.get('../api/joueurs/avancement/' + $scope.joueur._id).
                success(function(data, status, headers, config) {
                    $scope.aleatoire134 = data.aleatoire134;
                    $scope.aleatoire167 = data.aleatoire167;
                    $scope.aleatoire331 = data.aleatoire331;
                    $scope.aleatoire155 = data.aleatoire155;
                    afficherChoixAleatoires();

                }).
                error(function(data, status, headers, config) {
                    console.log("Error get avancement aleatoire: " + status);
                });
    }

    $scope.getNumPage = function(pageString) {
        var str = pageString.split("/");
        return str[2];
    };

    $scope.confirmerPickup = function() {
        $scope.setSection($scope.sectionPage + 1);


        $scope.joueur.objetsSpeciaux.push($scope.pickup);
        $http.put('../api/joueurs/' + $scope.joueur._id, {objetsSpeciaux: $scope.joueur.objetsSpeciaux});
        getPageHtml();
    };
    
    $scope.confirmerPickupSpecial = function(){
        for(var i = 0; i < $scope.pickupSpecial.length; ++i){
            var id = '#pick' + i;
            if($(id).prop('checked')){
                if($scope.numeroPage === 57){
                    $scope.joueur.objetsSpeciaux.push($scope.pickupSpecial[i]);
                }else{
                    $scope.joueur.objets.push($scope.pickupSpecial[i]);
                }      
            }
        }
        
        if($scope.numeroPage === 57){
        $http.put('../api/joueurs/' + $scope.joueur._id, {objetsSpeciaux: $scope.joueur.objetsSpeciaux});
    }
    else{
        $http.put('../api/joueurs/' + $scope.joueur._id, {objets: $scope.joueur.objets});
    }
        
        $scope.setSection($scope.sectionPage + 1);
        getPageHtml();
    };
    
    $scope.aRepas = function(){
        return $scope.joueur.objets.indexOf(objet.REPAS) >= 0;
    }
    
    $scope.perteRepas = function(){
        var i = $scope.joueur.objets.indexOf(objet.REPAS);
        if(i > -1){
        $scope.joueur.objets.splice(i, 1);
         $http.put('../api/joueurs/' + $scope.joueur._id, {objets: $scope.joueur.objets});
     }
            $scope.setSection($scope.sectionPage + 1);
            getPageHtml();
        
    };

    $scope.loadNewPage = function(page) {
        //get page infos
        $scope.numeroPage = page;
        $scope.sectionPage = 1;
        //on reset les trucs a afficher
        $scope.htmlPage = [];
        $scope.combat = {rondes: [], type: "combat"};
        $scope.round = 0;
        getPageHtml();

        if (page == 248 || page == 339) {
            $scope.joueur.endurancePlus = 0;
            $scope.joueur.enduranceBase = 0;
            updateAlive();
            return;
        }
        
        if ($scope.joueur.disciplines.indexOf(discipline.GUERISON) >= 0 && $scope.numeroPage != 1 && $scope.joueur.enduranceBase < $scope.joueur.enduranceMax) {
            $scope.joueur.enduranceBase += 1;
            $http.put('../api/joueurs/' + $scope.joueur._id, {enduranceBase: $scope.joueur.enduranceBase});
            alert("Vous avez gagner un point d'endurance grace a votre discipline Kai de guerison");
        }

        //gerer lavancement du joueur
        updateAvancement = {pageId: page, sectionId: 1};
        $http.put('../api/joueurs/avancement/' + $scope.joueur._id, updateAvancement);
    };

    $scope.setSection = function(section) {
        $scope.sectionPage = section;
        updateAvancement = {sectionId: section};
        $http.put('../api/joueurs/avancement/' + $scope.joueur._id, updateAvancement);
    }

    $scope.perteEndurance = function() {
        //checker le num de page et faire perdre de la vien en consequence
        if ($scope.numeroPage == 331 &&
                $scope.joueur.disciplines.indexOf(discipline.GUERISON) < 0) {
            $scope.joueur.enduranceBase -= 4;
            $scope.joueur.endurancePlus -= 4;
        }
        else if ($scope.numeroPage == 209) {
            $scope.joueur.enduranceBase -= 2;
            $scope.joueur.endurancePlus -= 2;
        }
        else if ($scope.numeroPage == 129 &&
                $scope.joueur.objetsSpeciaux.indexOf(objetSpecial.HUILE_DE_BAKANAL) < 0) {
            $scope.joueur.enduranceBase -= 3;
            $scope.joueur.endurancePlus -= 3;
        }else if ($scope.numeroPage == 155) {
            $scope.joueur.enduranceBase -= 3;
            $scope.joueur.endurancePlus -= 3;
            $scope.aConfirmation155 = false;
        }
        //on update le joueur sur le serveur
        $http.put('../api/joueurs/' + $scope.joueur._id, {enduranceBase: $scope.joueur.enduranceBase, endurancePlus: $scope.joueur.endurancePlus})
                .success(function(data, status, headers, config) {
                    updateAlive();
                    if ($scope.playerIsAlive) {
                        $scope.setSection(2);
                        $http.put('../api/joueurs/avancement/' + $scope.joueur._id, {sectionId: 2});
                        getPageHtml();
                    }
                }).
                error(function(data, status, headers, config) {
                    console.log("Error put player: " + status);
                });
    };

    getAvancementJoueur();
});
