pagesApp.controller('combatController', function ($scope, $http) {

	//:enduranceJoueur/:habileteJoueur/:enduranceMonstre/:habileteMonstre
    $scope.fuir = function(){
        $http.get('../api/combat/fuir/' + $scope.joueur.endurancePlus + '/' + $scope.joueur.habiletePlus + '/' + $scope.combat.endurance + '/' + $scope.combat.habilete + '/' + $scope.numeroPage).
		success(function(data) {
            $scope.joueur.enduranceBase -= parseInt(data.degatJoueur);
            $scope.joueur.endurancePlus -= parseInt(data.degatJoueur);
            $scope.combat.totalDomage += parseInt(data.degatJoueur);
            $scope.combat.rondes.push(data);
            
            //pour la page 180
            if($scope.numeroPage === 180){
                $scope.avancement.dommage180 = $scope.combat.totalDomage;
                updateAvancement = {dommage180: $scope.combat.totalDomage};
                $http.put('../api/joueurs/avancement/' + $scope.joueur._id, updateAvancement);
            }
            
            //on update le joueur sur le serveur
            $http.put('../api/joueurs/' + $scope.joueur._id, {enduranceBase : $scope.joueur.enduranceBase ,endurancePlus : $scope.joueur.endurancePlus})
            .success(function(data, status, headers, config){
                updateAlive();
                $scope.aCombat = false;
                if($scope.playerIsAlive){
					$scope.setSection(2);
					
					getPageHtml();
				}
            })
            .
            error(function(data, status, headers, config) {
                console.log("Error put player: " + status);
            });
            
            
		}).
		error(function(data, status, headers, config) {
			console.log("Error get decisions: " + status);
		});
    };	
    
    $scope.activerPP = function(){
      $scope.joueur.habiletePlus += 2;
      $scope.combat.aPP = false;
      $scope.combat.deltaHab = 2;
    }
    
    //:enduranceJoueur/:habileteJoueur/:enduranceMonstre/:habileteMonstre
    $scope.combattre = function(){

      
        $http.get('../api/combat/' + $scope.joueur.endurancePlus + '/' + $scope.joueur.habiletePlus + '/' + $scope.combat.endurance + '/' + $scope.combat.habilete + '/' + $scope.numeroPage).
		success(function(data) {
                  
            $scope.joueur.habiletePlus -= $scope.combat.deltaHab;
            $scope.combat.deltaHab = 0;
                  
            $scope.joueur.enduranceBase -= parseInt(data.degatJoueur);
            $scope.joueur.endurancePlus -= parseInt(data.degatJoueur);
            $scope.combat.endurance -= parseInt(data.degatEnnemi);
            $scope.combat.totalDomage += parseInt(data.degatJoueur);
            
            //pour la page 180
            if($scope.numeroPage === 180){
                $scope.avancement.dommage180 = $scope.combat.totalDomage;
                updateAvancement = {dommage180: $scope.combat.totalDomage};
                $http.put('../api/joueurs/avancement/' + $scope.joueur._id, updateAvancement);
            }
           
            $scope.combat.enVie = $scope.combat.endurance > 0;
            
            $scope.combat.rondes.push(data);
            //on update le joueur sur le serveur
            $http.put('../api/joueurs/' + $scope.joueur._id, {enduranceBase : $scope.joueur.enduranceBase ,endurancePlus : $scope.joueur.endurancePlus})
            .success(function(data, status, headers, config){
                updateAlive();
                $scope.aCombat = $scope.combat.enVie && $scope.playerIsAlive;
                if($scope.playerIsAlive && !$scope.aCombat){
					$scope.setSection(2);
					
					updateAvancement = {sectionId : 2};
					$http.put('../api/joueurs/avancement/' + $scope.joueur._id, updateAvancement);
					
					getPageHtml();
				}
            })
            .
            error(function(data, status, headers, config) {
                console.log("Error put player: " + status);
            });
            
            
		}).
		error(function(data, status, headers, config) {
			console.log("Error get decisions: " + status);
		});
    };	

})
