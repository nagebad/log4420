var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var combatSchema = new Schema({
    chiffreAleatoire: Number,
    enduranceMonstre: Number
});

var AvancementSchema = new Schema({
    pageId: Number,
    sectionId: Number,
    joueurId: Schema.Types.ObjectId,
    combats: [combatSchema],
    aleatoire134: Number,
	aleatoire167: Number,
	aleatoire331: Number,
    aleatoire155: Number,
    dommage180:Number
});

module.exports = mongoose.model('Avancement', AvancementSchema);

